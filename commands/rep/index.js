dataUtils = require('../../data_utils')

module.exports = {
    name: 'rep',
    description: 'Permite comprobar el estado actual de un usuario o del servidor en general:\n!rep\n!rep @usuario\n!rep list',
    execute: execute
}

function execute(msg, args, bot, db) {
    if (args.length > 0 && args[0] === 'list') {
        checkMemberList(msg, db)
    } else {
        let targetUserID = dataUtils.extractUserID(args)
        if (targetUserID && targetUserID !== msg.author.id) {
            checkOther(msg, targetUserID, db)
        } else {
            checkSelf(msg, db)
        }
        // Posible optimisation: Don't do this for each command, only once in a while
        checkAndUpdateGuild(msg.guild, db)
    }
}

function checkAndUpdateGuild(inputGuild, db) {
    db.findGuild(inputGuild.id).then((recoveredGuild) => {
        let finalGuild = dataUtils.extractGuild(recoveredGuild, inputGuild)
        db.upsertGuild(finalGuild).then(console.log("Guild updated!")).catch((e) => console.error(e))
    })
}

function checkSelf(msg, db) {
    db.findMember(msg.author.id, msg.guild.id).then((recoveredMember) => {
        let finalMember = dataUtils.extractMember(recoveredMember, msg.author, msg.guild)
        db.upsertMember(finalMember).then(console.log("Member updated!")).catch((e) => console.error(e))
        let nextLvlXP = finalMember.current_lvl * 10
        msg.reply(`eres nivel ${finalMember.current_lvl}. XP: ${finalMember.current_xp}/${nextLvlXP}. XP a repartir restantes: ${finalMember.xp_to_give}`)
    })
}

function checkOther(msg, targetUserID, db) {
    msg.guild.members.fetch(targetUserID).then((inputMember) => {
        db.findMember(inputMember.id, msg.guild.id).then((recoveredMember) => {
            let finalMember = dataUtils.extractMember(recoveredMember, inputMember.user, msg.guild)
            db.upsertMember(finalMember).then(console.log("Member updated!")).catch((e) => console.error(e))
            let nextLvlXP = finalMember.current_lvl * 10
            msg.reply(`${finalMember.username} es nivel ${finalMember.current_lvl}. XP: ${finalMember.current_xp}/${nextLvlXP}`)
        })
    })
}

function checkMemberList(msg, db) {
    db.findMembersOfGuild(msg.guild.id).toArray((err, membersList) => {
        let membersReply = buildMembersReply(membersList)
        msg.reply(`lista de reputaciones en este servidor: ${membersReply}`)
    })
}

function buildMembersReply(membersList) {
    let membersReply = ''
    membersList.forEach(member => {
        membersReply += `\n${member.username} - Nivel ${member.current_lvl}`
    });
    return membersReply
}