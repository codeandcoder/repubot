module.exports = {
    name: 'xp',
    description: 'Añade o resta xp a un usuario:\n!xp @usuario +1\n!xp @usuario -1',
    execute: execute
}

function execute(msg, args, bot, db) {
    let targetUserID = dataUtils.extractUserID(args)
    if (!targetUserID) {
        msg.reply('no se ha podido leer el nombre de usuario. Ejemplo de commando bien formateado: !xp @usuario +1')
        return
    }

    if (args.length < 2) {
        msg.reply('no se ha podido leer el comando. Ejemplo de commando bien formateado (los espacios son importantes!): !xp @usuario +1')
    }

    db.findMember(msg.author.id, msg.guild.id).then(member => {
        if (member && member.xp_to_give <= 0) {
            msg.reply('ya no te quedan xp que repartir, se recargarán hoy a las 12 de la noche.')
        } else {
            msg.guild.members.fetch(targetUserID).then((inputMember) => {
                getTargetUser(inputMember.user, msg.guild, db).then(targetUser => {
                    if (targetUser.id === msg.author.id) {
                        msg.reply('no puedes cambiarte tus propios xp!')
                    } else {
                        if (args[1] === '+1') {
                            addXPToUser(targetUser, msg, db).then(() => {
                                spendXPToGive(msg.author, msg.guild, db).then(()=> {
                                    msg.reply(`${targetUser.username} ha recibido tu apoyo!`)
                                }).catch(e => {
                                    console.error(e)
                                })
                            }).catch(e => {
                                console.error(e)
                            })
                        } else if (args[1] === '-1') {
                            subtractXPToUser(targetUser, msg, db).then(() => {
                                spendXPToGive(msg.author, msg.guild, db).then(()=> {
                                    msg.reply(`${targetUser.username} ha recibido tu penalización!`)
                                }).catch(e => {
                                    console.error(e)
                                })
                            }).catch(e => {
                                console.error(e)
                            })
                        } else {
                            msg.reply('solo se puede sumar o restar 1 punto de experiencia cada vez. Ejemplo: !xp @usuario +1')
                        }
                    }
                }).catch(e => {
                    console.error(e)
                })
            }).catch(e => {
                console.error(e)
            })
        }
    }).catch(e => {
        console.error(e)
    })
}

function getTargetUser(inputMember, guild, db) {
    return new Promise((resolve, reject) => {
        db.findMember(inputMember.id, guild.id).then((recoveredMember) => {
            let finalMember = dataUtils.extractMember(recoveredMember, inputMember, guild)
            db.upsertMember(finalMember).then(() => {
                console.log("Member updated!")
                resolve(finalMember)
            }).catch(e => reject(e))
        }).catch(e => {
            reject(e)
        })
    })
}

function addXPToUser(targetUser, msg, db) {
    return new Promise((resolve, reject) => {
        if (targetUser.current_xp + 1 >= targetUser.current_lvl * 10) {
            targetUser.current_xp = 0
            targetUser.current_lvl += 1
            msg.channel.send(`<@!${targetUser.id}> ha subido al nivel ${targetUser.current_lvl}!!! :)`)
        } else {
            targetUser.current_xp += 1
        }
        db.upsertMember(targetUser).then(() => {
            console.log("Member updated!")
            resolve()
        }).catch(e => reject(e))
    })
}

function subtractXPToUser(targetUser, msg, db) {
    return new Promise((resolve, reject) => {
        if (targetUser.current_xp - 1 <= 0) {
            if (targetUser.current_lvl > 1) {
                targetUser.current_lvl -= 1
                targetUser.current_xp = targetUser.current_lvl * 10 - 1
                msg.channel.send(`<@!${targetUser.id}> ha bajado al nivel ${targetUser.current_lvl}!!! :(`)
            }
        } else {
            targetUser.current_xp -= 1
        }
        db.upsertMember(targetUser).then(() => {
            console.log("Member updated!")
            resolve()
        }).catch(e => reject(e))
    })
}

function spendXPToGive(user, guild, db) {
    return new Promise((resolve, reject) => {
        getTargetUser(user, guild, db).then(targetUser => {
            targetUser.xp_to_give -= 1
            db.upsertMember(targetUser).then(() => {
                console.log("Member updated!")
                resolve()
            }).catch(e => reject(e))
        }).catch(e => {
            reject(e)
        })
    })
}