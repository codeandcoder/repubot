exports.extractGuild = function(savedGuild, inputGuild) {
    return {
        id: inputGuild.id,
        name: inputGuild.name,
        config: {
            language: savedGuild ? savedGuild.config.language : 'en'
        }
    }
}

exports.extractMember = function(savedMember, inputMember, guild) {
    return {
        id: inputMember.id,
        guild_id: guild.id,
        username: inputMember.username,
        current_xp: savedMember ? savedMember.current_xp : 0,
        current_lvl: savedMember ? savedMember.current_lvl : 1,
        xp_to_give: savedMember ? savedMember.xp_to_give : 3
    }
}

exports.extractUserID = function(args) {
    if (args.length > 0) {
        let users = args[0].match(/<@!(.*)>/gm)
        if (users.length > 0) {
            return users[0].substring(3, users[0].length-1)
        }
    }
    return null
}