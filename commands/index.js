module.exports = {
    Help: require('./help'),
    GetRank: require('./rep'),
    GiveXP: require('./xp')
}