module.exports = {
    name: 'help',
    description: 'Muestra esta ayuda.',
    execute: execute
}

function execute(msg, args, bot, db) {
    let helpReply = 'Comandos disponibles:'
    bot.commands.forEach(c => {
        helpReply += `\n\n${bot.actionCharacter}${c.name} - ${c.description}`
    })
    msg.reply(helpReply)
}