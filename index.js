require('dotenv').config()

const dataUtils     = require('./data_utils')
const schedule      = require('node-schedule')
const mongodb       = require('./mongodb')
const discord       = require('discord.js')
const bot           = new discord.Client()
bot.commands        = new discord.Collection()
const botCommands   = require('./commands')

const DISCORD_TOKEN = process.env.DISCORD_TOKEN
const MONGODB_TOKEN = process.env.MONGODB_TOKEN
bot.actionCharacter = '!'

bot.login(DISCORD_TOKEN)
mongodb.connect(MONGODB_TOKEN).then(() => {
    bot.on('ready', () => {
        console.info(`Logged in as ${bot.user.tag}!`)
    })
    
    Object.keys(botCommands).map(key => {
        bot.commands.set(botCommands[key].name, botCommands[key])
    })
    
    bot.on('message', msg => {
        if (msg.content.charAt(0) === bot.actionCharacter) {
            const args = msg.content.split(/ +/)
            const command = args.shift().toLowerCase().substr(1)
        
            if (!bot.commands.has(command)) return
    
            console.info(`Command \'${msg.content}\' called by ${msg.author.username}`)
            try {
                bot.commands.get(command).execute(msg, args, bot, mongodb)
            } catch (error) {
                console.error(error)
                msg.reply('there was an error trying to execute that command!')
            }
        }
    })
}).catch(e => {
    console.error(e)
})

// Schedule replenishment of xp to give
var everyDayAt24= "00 22 * * *"
var replenishmentJob = schedule.scheduleJob(everyDayAt24, replenishXP)

function replenishXP() {
    console.log('Replenishing xp...')
    mongodb.findAllMembers().toArray((err, membersList) => {
        let upsertArray = []
        membersList.forEach(member => {
            member.xp_to_give = calculateXPToGive(member)
            upsertArray.push(new Promise((resolve, reject) => {
                mongodb.upsertMember(member).then(resolve).catch((e) => reject(e))
            }))
        })
        Promise.all(upsertArray).then(console.log('Replenishment finished!'))
    })
}

function calculateXPToGive(user) {
    if (user.current_lvl < 3) {
        return 3
    } else if (user.current_lvl < 6) {
        return 4
    } else if (user.current_lvl < 10) {
        return 5
    }
    return 6
}