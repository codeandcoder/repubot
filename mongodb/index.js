const MongoClient   = require('mongodb').MongoClient
var client          = null
var db              = null

exports.connect = function(mongodbToken) {
    return new Promise((resolve, reject) => {
        client = new MongoClient(mongodbToken, { useUnifiedTopology: true })
        client.connect().then(() => {
            db = client.db('repubot')
            console.info("Connected to DB successfuly!")
            resolve()
        }).catch((e) => {
            reject(e)
        })
    })
}

exports.close = function() {
    client.close()
}

exports.findGuild = function(id) {
    return db.collection('guilds').findOne({id:id})
}

exports.upsertGuild = function(guild) {
    return db.collection('guilds').replaceOne({id:guild.id}, guild, {upsert:true})
}

exports.findMember = function(member_id, guild_id) {
    return db.collection('members').findOne({id:member_id, guild_id:guild_id})
}

exports.upsertMember = function(member) {
    return db.collection('members').replaceOne({id:member.id, guild_id:member.guild_id}, member, {upsert:true})
}

exports.findMembersOfGuild = function(guild_id) {
    return db.collection('members').find({guild_id:guild_id})
}

exports.findAllMembers = function() {
    return db.collection('members').find()
}